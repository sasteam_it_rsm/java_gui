package radar_system.devices;

import radar_system.common.*;

public class ButtonPressedOn implements Event {
	private Button source;
	
	public ButtonPressedOn(Button source){
		this.source = source;
	}
	
	public Button getSourceButton(){
		return source;
	}
}
