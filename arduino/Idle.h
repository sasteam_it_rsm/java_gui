#ifndef __IDLE__
#define __IDLE__

#include "Task.h"

class Idle: public Task {  
  public:
    Idle(int led);
    void init(int period);
    void tick();
};


#endif
