package radar_system.devices;

public interface Button {
	
	boolean isPressed();

}
