#ifndef __MOVING__
#define __MOVING__

#include "Task.h"
#include "Sonar.h"
#include "Led.h"
#include "ServoTimer2.h"


class Moving: public Task{   
  Led* led;
  Sonar* prox;
  ServoTimer2* servo;


  public:
    Moving(int greenLed, int servo, int echo, int trigger, int servoSpeed);
    void init(int period);
    void tick();

};


#endif
