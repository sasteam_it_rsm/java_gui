package radar_system.event;

import com.pi4j.io.gpio.*;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;

import radar_system.common.*;
import radar_system.devices.*;
import radar_system.devices.p4j_impl.Config;

public class ButtonOn extends ObservableButton {

    private GpioPinDigitalInput pin;

    public ButtonOn(int pinNum) {
        super();
        try {
            GpioController gpio = GpioFactory.getInstance();
            pin = gpio.provisionDigitalInputPin(Config.pinMap[pinNum], PinPullResistance.PULL_DOWN);
        } catch (Exception e) {
            e.printStackTrace();
        }

        pin.addListener(new ButtonListener(this));
    }

    @Override
    public synchronized boolean isPressed() {
        return pin.isHigh();
    }

    class ButtonListener implements GpioPinListenerDigital {
        ButtonOn button;

        public ButtonListener(ButtonOn button) {
            this.button = button;
        }

        public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
            Event ev = null;
            
            if (event.getState().isHigh()) {
                ev = new ButtonPressedOn(button);
            } else {
                ev = new ButtonReleasedOn(button);
            }
            notifyEvent(ev);
        }
    }
}
