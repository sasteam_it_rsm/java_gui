package radar_system.event;

import radar_system.common.Event;

public class MessageIncoming implements Event {

	private String msg;

	public MessageIncoming(String mess) {
		this.msg = mess;
	}

	@Override
	public String toString() {
		return "MessageIncoming [" + this.msg + "]";
	}

	public String getMsg() {
		return this.msg;
	}

}
