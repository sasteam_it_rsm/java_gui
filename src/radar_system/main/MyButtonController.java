package radar_system.main;

import java.io.IOException;

import radar_system.common.*;
import radar_system.devices.*;
import radar_system.event.EndTracking;
import radar_system.event.ObjDetected;
import radar_system.event.StartTracking;

public class MyButtonController extends ReactiveAgent {

	private Light ledOn;
	private Light ledDetected;
	private Light ledTracking;
	private ObservableButton buttonOn;
	private ObservableButton buttonOff;
	private ReactiveAgent peer;

	public MyButtonController(ObservableButton buttonOn,
			ObservableButton buttonOff, Light LedOn, Light LedDetected,
			Light LedTracking) {
		this.ledOn = LedOn;
		this.ledDetected = LedDetected;
		this.ledTracking = LedTracking;
		this.buttonOn = buttonOn;
		this.buttonOff = buttonOff;
		buttonOn.addObserver(this);
		buttonOff.addObserver(this);
		try {
			ledOn.switchOff();
			ledDetected.switchOff();
			ledTracking.switchOff();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected void processEvent(Event ev) {
		try {
			if (ev instanceof ButtonPressedOn) {
				ledOn.switchOn();
				peer.notifyEvent(ev);
			} else if (ev instanceof ButtonPressedOff) {
				ledOn.switchOff();
				peer.notifyEvent(ev);
			} else if (ev instanceof ObjDetected) {
				ledDetected.switchOn();
				Thread.sleep(100);
				ledDetected.switchOff();
			} else if (ev instanceof StartTracking) {
				ledTracking.switchOn();
			} else if (ev instanceof EndTracking) {
				ledTracking.switchOff();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public void init(ReactiveAgent peer) {
        this.peer = peer;
    }
}
