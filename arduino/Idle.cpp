#include "Idle.h"
#include "Task.h"
#include "Led.h"
#include <Arduino.h>

#define DIST_MIN 0.1
#define DIST_MAX 1
#define DIST_CLOSE 0.5
#define VALUES 5
#define DIFF 0.3

extern int state;
Led *greenLed;

Idle::Idle(int led) {
   greenLed = new Led(led);
}

void Idle::init(int period) {
  Task::init(period);
}

void Idle::tick() {
  if((int)Serial.read()==79) {
    state = 1;
    greenLed->switchOn();
  }
}

  
  
  
  
  
  

