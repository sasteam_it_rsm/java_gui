package radar_system.main;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import radar_system.common.Event;
import radar_system.common.ReactiveAgent;
import radar_system.devices.ButtonPressedOff;
import radar_system.devices.ButtonPressedOn;
import radar_system.event.EndTracking;
import radar_system.event.MessageIncoming;
import radar_system.event.ObjDetected;
import radar_system.event.StartTracking;

public class MySerialController extends ReactiveAgent {

	private SerialMonitor sm;
	private ReactiveAgent peer;
	private static final Double DIST_DETECTING = 0.50;
	private static final Double DIST_TRACKING = 0.20;
	private static final Double MAX_DIST = new Double(1);
	private int countDetectedObjs = 0;
	private boolean check = false;
	private boolean write = false;

	public MySerialController() throws IOException {
		this.sm = new SerialMonitor(this);
		this.sm.start("/dev/ttyACM0", 9600);
		//this.sm.write('O');
		MyFileWriter.createFile();
	}

	@Override
	protected void processEvent(Event ev) {
		try {
			if (ev instanceof MessageIncoming) {
				final MessageIncoming msg = (MessageIncoming) ev;
				int degrees = Integer.parseInt(msg.getMsg().split(":")[0]);
				Double distance = Double
						.parseDouble(msg.getMsg().split(":")[1]);
				
				if(distance > MAX_DIST) {
					distance = MAX_DIST;
				}
				
				

				if (check && distance > DIST_TRACKING) {
					peer.notifyEvent(new EndTracking());
					this.sm.write('R');
					check = false;
				}

				if (check) {
					DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
					Date dateobj = new Date();
					
					System.out.println("Time " + df.format(dateobj)
							+ " - Object tracked at angle " + degrees
							+ " distance " + distance);
					
					MyFileWriter.writeLine("Time " + df.format(dateobj)
							+ " - Object tracked at angle " + degrees
							+ " distance " + distance);
				}

				if (distance <= DIST_DETECTING) {
					if (distance <= DIST_TRACKING && !check) {
						peer.notifyEvent(new StartTracking());
						this.sm.write('P');
						check = true;
					} else if (distance > DIST_TRACKING) {
											
						DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
						Date dateobj = new Date();
						
						System.out.println("Time " + df.format(dateobj)
								+ " - Object detected at angle " + degrees);
						
						MyFileWriter.writeLine("Time " + df.format(dateobj)
								+ " - Object detected at angle " + degrees);
						
						countDetectedObjs++;

						peer.notifyEvent(new ObjDetected());

					}
				}

				if (!write && (degrees == 180 || degrees == 0)) {
					System.out.println("Scan completed, "
							+ countDetectedObjs + " objects found!");
					
					countDetectedObjs = 0;
				}
				

			} else if (ev instanceof ButtonPressedOn) {
				this.sm.write('O');

			} else if (ev instanceof ButtonPressedOff) {
				this.sm.write('F');
			}
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}

	}
	
	public void init(ReactiveAgent peer) {
        this.peer = peer;
    }

}
