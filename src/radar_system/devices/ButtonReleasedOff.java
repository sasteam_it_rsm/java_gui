package radar_system.devices;

import radar_system.common.*;

public class ButtonReleasedOff implements Event {
	private Button source;
	
	public ButtonReleasedOff(Button source){
		this.source = source;
	}
	
	public Button getSourceButton(){
		return source;
	}
}
