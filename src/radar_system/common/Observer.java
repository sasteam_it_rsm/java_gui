package radar_system.common;

public interface Observer {

	boolean notifyEvent(Event ev);
}
