package radar_system.main;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;

public class MyFileWriter {

	private static final MyFileWriter LOG = new MyFileWriter();
	private static final Path file = Paths.get("/home/pi/Desktop/test.txt");
	private static final Charset utf8 = StandardCharsets.UTF_8;

	private MyFileWriter() {
	}

	public static MyFileWriter getWriter() {
		return LOG;
	}

	/**
	 * Creates the file to be written. Default path: user home
	 * 
	 * @throws IOException
	 *             if an error occurs while creating the file
	 */
	public static void createFile() throws IOException {
		if (!exists()) {
			Files.write(file, "".getBytes());
		}
	}

	/**
	 * Writes a single line into the file (append).
	 * 
	 * @throws IOException
	 *             if an error occurs while creating the file
	 */
	public static void writeLine(String arg) throws IOException {
		if (!exists()) {
			throw new IllegalStateException(
					"The file has not been created yet.");
		}

		List<String> lines = Arrays.asList(arg);
		Files.write(file, lines, utf8, StandardOpenOption.CREATE,
				StandardOpenOption.APPEND);
	}

	/**
	 * Writes more lines into the file (append).
	 * 
	 * @throws IOException
	 *             if an error occurs while creating the file
	 */
	public static void writeLines(String... args) throws IOException {
		if (!exists()) {
			throw new IllegalStateException(
					"The file has not been created yet.");
		}
		Files.write(file, Arrays.asList(args), utf8, StandardOpenOption.CREATE,
				StandardOpenOption.APPEND);
	}

	private static boolean exists() {
		return Files.exists(file);
	}

}
