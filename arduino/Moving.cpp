#include "Moving.h"
#include "Task.h"
#include <Arduino.h>

#define MIN 544
#define MAX 2100
#define MIDDLE 1321


extern int state;
int grLed, ser, ech, trig, srvSpeed;
int conversion=0;
int x;
bool go=true;
bool tracking=false;

Moving::Moving(int greenLed, int servo, int echo, int trigger, int servoSpeed) {
  grLed = greenLed;
  ser = servo;
  ech = echo;
  trig = trigger;
  srvSpeed = servoSpeed;
}

void Moving::init(int period) {
  Task::init(period);
  led = new Led(grLed);
  prox = new Sonar(ech, trig);
  servo = new ServoTimer2();
  conversion=MIDDLE;
  servo->attach(ser);
  servo->write(MIDDLE);
}

void Moving::tick() {
  int tmp=(int)Serial.read();
  //OFF
  if(tmp==70) { 
    state = 0;
    led->switchOff();
    
  }
  //P
  if(tmp==80){ 
    go=false;
    tracking=true;    
    
  }
  //R
   if(tmp==82){
    go=true;
    tracking=false;   
  } 
  
  if(tracking){
    Serial.print(x);
    Serial.print(":");
    Serial.println(prox->getDistance());
  }
  
  if(go){
    conversion+=srvSpeed;
    x = map(conversion,MIN,MAX,0,180);
    Serial.print(x);
    Serial.print(":");
    Serial.println(prox->getDistance());
    if(conversion>MAX || conversion<MIN){
      srvSpeed=-srvSpeed;
    }
    servo->write(conversion);
  }
}
