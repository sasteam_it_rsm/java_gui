/*
 * Sviluppato da Carlo Noia, Jacopo Conti, Leonardo Papini.
 */
#include "ServoTimer2.h"
#include <Arduino.h>
#include "Timer.h"
#include "Idle.h"
#include "Moving.h"

#define GREEN_LED 5
#define SERVO 6
#define ECHO 7
#define TRIGGER 8
#define OMEGA 10

Timer timer;
Idle idle(GREEN_LED);
Moving moving(GREEN_LED, SERVO, ECHO, TRIGGER, OMEGA);
int state;

void setup() {
  state = 0;
  timer.setupPeriod(10);
  idle.init(50);
  moving.init(50);
  Serial.begin(9600);
}

void loop() {
  timer.waitForNextTick();
  if(state == 0) {
    if(idle.updateAndCheckTime(10)) {
      idle.tick();
    }    
  }
  else {
    if(moving.updateAndCheckTime(10)){
      moving.tick();
    }
      
    
  }
}
