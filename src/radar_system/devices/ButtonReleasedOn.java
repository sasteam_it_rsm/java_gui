package radar_system.devices;

import radar_system.common.*;

public class ButtonReleasedOn implements Event {
	private Button source;
	
	public ButtonReleasedOn(Button source){
		this.source = source;
	}
	
	public Button getSourceButton(){
		return source;
	}
}
