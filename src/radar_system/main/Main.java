/*
 * Sviluppato da Carlo Noia, Jacopo Conti, Leonardo Papini.
 */
package radar_system.main;

import java.io.IOException;

import radar_system.devices.Light;
import radar_system.devices.ObservableButton;

public class Main  {



	public static void main(String[] args) {

		Light LedOn = new radar_system.devices.p4j_impl.Led(0);
		Light LedDetected = new radar_system.devices.p4j_impl.Led(2);
		Light LedTracking = new radar_system.devices.p4j_impl.Led(3);

		ObservableButton buttonOn = new radar_system.event.ButtonOn(
				4);
		ObservableButton buttonOff = new radar_system.event.ButtonOff(
				5);

		MySerialController mySrlCtrl = null;
		try {
			mySrlCtrl = new MySerialController();
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
		MyButtonController btnCtrl = new MyButtonController(buttonOn,
				buttonOff, LedOn, LedDetected, LedTracking);

		btnCtrl.init(mySrlCtrl);
		mySrlCtrl.init(btnCtrl);
		
		
		btnCtrl.start();
		mySrlCtrl.start();
		

		

	}

}
