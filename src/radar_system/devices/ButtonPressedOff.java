package radar_system.devices;

import radar_system.common.*;

public class ButtonPressedOff implements Event {
	private Button source;
	
	public ButtonPressedOff(Button source){
		this.source = source;
	}
	
	public Button ButtonPressedOff(){
		return source;
	}
}
