package radar_system.main;

import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;

import radar_system.common.BasicEventLoopController;
import radar_system.common.Event;
import radar_system.event.MessageIncoming;

/**
 * Simple Serial Monitor, adaptation from:
 * 
 * http://playground.arduino.cc/Interfacing/Java
 * 
 */
public class SerialMonitor implements SerialPortEventListener {
	SerialPort serialPort;

	/**
	 * A BufferedReader which will be fed by a InputStreamReader converting the
	 * bytes into characters making the displayed results codepage independent
	 */
	private BufferedReader input;
	/** The output stream to the port */
	private OutputStream output;
	/** Milliseconds to block while waiting for port open */
	private static final int TIME_OUT = 2000;

	private BasicEventLoopController observer;

	public SerialMonitor(BasicEventLoopController observer) {
		this.observer = observer;

	}

	public void start(String portName, int dataRate) {
		CommPortIdentifier portId = null;

		try {
			portId = CommPortIdentifier.getPortIdentifier(portName);
			// open serial port, and use class name for the appName.
			serialPort = (SerialPort) portId.open(this.getClass().getName(),
					TIME_OUT);

			// set port parameters
			serialPort.setSerialPortParams(dataRate, SerialPort.DATABITS_8,
					SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);

			// open the streams
			input = new BufferedReader(new InputStreamReader(
					serialPort.getInputStream()));
			output = serialPort.getOutputStream();

			// add event listeners
			serialPort.addEventListener(this);
			serialPort.notifyOnDataAvailable(true);

		} catch (Exception e) {
			System.err.println(e.toString());
		}
	}

	/**
	 * This should be called when you stop using the port. This will prevent
	 * port locking on platforms like Linux.
	 */
	public synchronized void close() {
		if (serialPort != null) {
			serialPort.removeEventListener();
			serialPort.close();
		}
	}

	/**
	 * Handle an event on the serial port. Read the data and print it.
	 */
	@Override
	public synchronized void serialEvent(SerialPortEvent oEvent) {
		if (oEvent.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
			try {
				String inputLine = input.readLine();
				//System.out.println(inputLine);

				if (observer != null) {
					observer.notifyEvent(new MessageIncoming(inputLine));
				}
			} catch (Exception e) {
			}
		}
	}

	public void write(char character) {
		try {
			output.write((int) character);
		} catch (IOException e) {
		}
	}

	
}